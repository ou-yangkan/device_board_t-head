# CB2201

## 介绍

CB2201是平头哥一款基于CH2201芯片的物联网应用开发板，开发板提供了JTAG仿真调试接口，按键，LED指示灯及扩展接口，扩展接口支持GPIO、I2C、ADC、UART、SPI、WiFi/Ethernet模组、传感器等。

CH2201芯片内核是基于平头哥玄铁802 32-bit CPU，内置80k SRAM和256k flash。

开发板实物图片：

![cb2201实物图](https://gitee.com/openharmony-sig/device_board_t-head/raw/master/figures/cb2201实物图.jpg)

开发板功能块图：

![cb2201功能块图](https://gitee.com/openharmony-sig/device_board_t-head/raw/master/figures/cb2201接口图.png)

注：此图来自官方sdk中的CSI-RTOS_CB2201_SDK(v1.6.3)_User_Guide，具体模块或接口介绍请参考官方文档。

## 开发板规格

| 器件类别 | 开发板                                   |
| -------- | --------------------------------------- |
| CPU      | 32BIT T-HEAD CK802T 处理器（最高48MHz）  |
| SRAM     | 80 KB                                   |
| ROM      | 256 KB                                  |
| GPIO     | 34                                      |
| I2C      | 2                                       |
| UART     | 3                                       |
| SPI      | 2                                       |
| I2S      | 1                                       |
| ADC      | 16                                      |
| ACMP     | 1                                       |
| PWM      | 4                                       |

## 搭建开发环境

### 系统要求

系统要求基于C-SKY结构的liteos_m内核操作系统，采用平头哥官方提供的csky-elfabiv2-gcc（gcc 版本 6.3.0）的toolchain。

OpenHarmony需要按照官方文档介绍安装环境https://gitee.com/openharmony-sig/device_soc_t-head/blob/master/README.md，然后编译出烧录文件，按照文档介绍烧录。

### 工具要求

ubuntu 18.04编译，windows10系统烧录。

https://gitee.com/openharmony-sig/device_soc_t-head/blob/master/README.md

### 搭建过程

https://gitee.com/openharmony-sig/device_soc_t-head/blob/master/README.md

## 编译调试

https://gitee.com/openharmony-sig/device_soc_t-head/blob/master/README.md

按下开发板上reset power按钮，通过串口调试工具打印串口log。

## 首个示例

代码默认有一个UART打印示例。

## 参考资源

使用教程：https://gitee.com/openharmony-sig/device_soc_t-head/blob/master/README.md

官方sdk等资料下载地址：https://occ.t-head.cn/vendor/detail/download?spm=a2cl5.14301546.0.0.1337180fl09i56&id=635878225301471232&vendorId=3706716635429273600&module=4#sticky
